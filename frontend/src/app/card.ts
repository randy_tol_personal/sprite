export class Card {
  constructor(index:number, color: string, face: string) {
    this.index = index;
    this.color = color;
    this.face = face;
  }

  index: number;
  color: string;
  face: string;
}
