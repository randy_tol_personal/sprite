export enum FACES{
  UNO = '1',
  DOS = '2',
  TRES = '3',
  QUATRO = '4',
  SINGKO = '5',
  SAES = '6',
  SIETE = '7',
  OTSO = '8',
  NUEVE = '9',
  ZERO = '0',
  SKIP = 'skip',
  REVERSE = 'reverse',
  PLUS2 = 'plus2',
  PLUS4 = 'plus4',
  WILDCARD = 'wildcard',
  ANY = 'any'
}
