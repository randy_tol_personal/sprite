import { TestBed } from '@angular/core/testing';

import { StompMessagingService } from './stomp-messaging.service';

describe('StompMessagingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StompMessagingService = TestBed.get(StompMessagingService);
    expect(service).toBeTruthy();
  });
});
