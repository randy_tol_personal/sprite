import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {StompMessagingService} from "../stomp-messaging.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  @Input()
  name: string;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  goTopage(page: string): void{
    this.router.navigate([page]);
  }

  pressEnter($event: KeyboardEvent) {
    console.log($event);
    if($event.code == 'Enter')
      this.goTopage(`/play/${this.name}`);
  }
}
