import {Injectable} from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

@Injectable({
  providedIn: 'root'
})
export class StompMessagingService {
  private serverUrl = 'http://localhost:8080/sprite-websocket';
  public stompClient;

  constructor() {
    this.initializeWebSocketConnection();
  }

  initializeWebSocketConnection(): void {
    const ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);

    this.stompClient.connect({}, frame => {
      /*this.stompClient.subscribe('/sprite', (message) => {
        console.log(message.body);

        /!*if (message.body) {
          console.log(typeof message.body);

          const msg = message.body;
          switch (msg.action) {
            case 'login':
              console.log('welcome ' + msg.username);
              break;
          }
        }*!/
      });*/
    });
  }

  sendMessage(act: string, message: any) {
    switch (act) {
      case 'login':
        this.stompClient.send('/app/login/username' , {}, message);
        break;
      case 'prepare':
        this.stompClient.send('/app/prepare/usersAndCards' , {}, message);
        break;
      case 'place':
        this.stompClient.send('/app/place/card' , {}, message);
        break;
    }

  }
}
