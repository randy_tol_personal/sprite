import {Component, OnInit} from '@angular/core';
import {Card} from '../card';
import {COLOR} from '../color';
import {FACES} from '../faces';
import {ActivatedRoute} from '@angular/router';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {ISALLOWED} from "../css-classes-allow";

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.less', '../home/home.component.less']
})
export class PlayComponent implements OnInit {
  private timeOutTime: number=3000;

  constructor(
    private route: ActivatedRoute
    ) {
      this.initializeWebSocketConnection();
    }

  //private serverUrl = 'http://ec2-52-15-62-220.us-east-2.compute.amazonaws.com:8080/sprite-websocket';
  private serverUrl = 'http://localhost:8080/sprite-websocket';
  public stompClient;

  users: any[] = [];
  cards: Card[] = [];
  myCards: Card[] = [];
  piled: Card;
  rotation: number;
  isClockWise:boolean = true;
  noMoves:boolean = false;

  const warnPlus4 = 'By placing this card, you understand that you might be challenged by the next player that ' +
    'you still have another card to place, aside from this. If you placed this card and the challenged is on, ' +
    'you have to get 4 more cards as consequence.\n' +
    '\n' +
    'On the other hand, the challenge may still be opted by the next player, even if you dont have any valid ' +
    'cards aside from this, if the player did, the player will get 6 more cards.';

  iam: {isMe: boolean, cards: number, name: string, index: number} = {isMe: true, cards: 0, name: '', index: 0};
  myName: string;
  usersAndDeck: {users: any, deck: Card[]} = {users: null, deck: []};

  private whichComesFirst = 1;
  message: string;
  initializing = '';
  initCount = 1;
  stopInit = false;
  fcs: FACES;
  connectionSuccess = false;

  initAnimate(ii: number) {
    console.log(ii + this.initializing);
    if (ii >= 5) {
      ii = 0;
      this.initializing = '';
    }

    this.initializing += '.';

    if (this.cards.length <= 0) {
      setTimeout(() => this.initAnimate(ii + 1), 500);
    } else {
      this.initializing = '';
    }
  }

  initializeCards(): void {
    let x = 0;
    for (const i in COLOR) {
      const color = COLOR[i];
      console.log(color);

      for (const j in FACES) {
        const face = FACES[j];
        if (face != FACES.ANY) {
          const card = new Card(null, null, null);

          if (face == FACES.WILDCARD || face == FACES.PLUS4) {
            card.color = null;
          } else {
            card.color = color;
          }

          card.index = x++;
          card.face = face;

          this.cards.push(card);
        }
      }
    }

    // for ANY
    this.cards.push(new Card(x++, null, FACES.ANY));

    console.log(this.cards);
  }

  shuffleDeck(): void {
    this.cards = this.shuffleArray(this.cards);
    // console.log(this.cards);
  }

  getShuffledDeck(howMany: number, shuffle: boolean): Card[] {
    if (shuffle) {
      this.shuffleDeck();
    }

    // get a copy of first 7 for example
    const myShuffledDeck = this.cards.slice(0, howMany);

    // then pop / shift cards (remaining deck)
    for (let i = 0; i < howMany; i++) {
      this.cards.shift();
    }
    console.log(this.cards);

    console.log(myShuffledDeck);
    return myShuffledDeck;
  }

  // -> Fisher–Yates shuffle algorithm
  shuffleArray(arr: any[]): any[] {
    let m = arr.length, t, i;

    // While there remain elements to shuffle
    while (m) {
      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);

      // And swap it with the current element.
      t = arr[m];
      arr[m] = arr[i];
      arr[i] = t;
    }

    console.log(arr);
    return arr;
  }

  popupExit() {
    /*TODO: make a popup confirmation*/
  }

  gameBegun() {
    // send error that game has already begun
  }

  updateUsers() {

  }

  beginGame() {
    // updateUsers
  }

  reverseRotation() {
    /*TODO internally make the rotation increment*/
  }

  doUpdatePile(index: number) {
    // WebSocket pick up the card and update pile then do the turn
  }

  doTheLogic(card: Card) {
    // TODO logic for each card
  }

  playAndSend(card: Card) {
    // do any logic then websocket send update pile
    const allowPlay = this.checkIfAllowed(card);
    if(allowPlay==ISALLOWED.NORMAL || allowPlay == ISALLOWED.NORMALHELP){
      this.piled = card;
      this.myCards.splice(this.myCards.indexOf(card),1);
      this.checkIfNoMoves(this.myCards);
    }
  }

  drawAndQueueToMyDeck(){
    if(this.noMoves){
      const drawCard = this.cards[0];
      this.myCards.unshift(drawCard);
      this.checkIfNoMoves([drawCard]);
      this.cards.shift();
      //websocket message updateDeckByNumber(nth)
    }
  }

  checkIfNoMoves(checkThisCards: Card[]){
    this.noMoves=true;
    for(let i in checkThisCards){
      if(this.checkIfAllowed(checkThisCards[i])==ISALLOWED.NORMAL){
        this.noMoves=false;
        break;
      }
    }
  }

  myTurn(){
    //this.noMove=true;
  }

  drawAndPile() {
    console.log('Draw and Pile');
    let continuePile: boolean;

    do {
      const toPile = this.cards[0];
      //toPile.face = FACES.ZERO;
      console.log(toPile);
      // @ts-ignore
      const face = +toPile.face;
      if (face >= 0 && face <= 9) {
        this.piled = toPile;
        continuePile = false;

      } else {
        continuePile = true;
        this.cards.push(toPile);
      }
      this.cards.shift();

    }while (continuePile);
  }

  messageAppear(msg: string) {
    this.message = msg;
    setTimeout(() => {this.message = ''; }, this.timeOutTime);
  }

  doLogin(msg: any): void {
    console.log('welcome ' + msg.username);

    if (this.myName.toLowerCase() != msg.username.toLowerCase()) {

      // SEND REQUEST FOR PREPARE FOR NEW LOGGED IN USER
      console.log('OTHER USER LOGGING IN.. sending request ' + this.whichComesFirst++);
      this.messageAppear(msg.username.toUpperCase() + ' joined the Game...');

      const otherUser = {isMe: false, cards: 7, name: msg.username.toUpperCase(), index: this.users.length};
      this.users.push(otherUser);

      const toSend = this.cards.map(x => x.index + '');
      toSend.push(`${this.piled ? this.piled.index : -1}`);
      toSend.push(this.myName.toUpperCase());
      toSend.push(msg.username.toUpperCase());

      console.log('Cards Prepared');
      console.log(this.cards);

      try {
        this.sendMessage('prepare', JSON.stringify(toSend.reverse()));
      } catch (e) {
        console.log(e);
      }
    } else {
      // LISTEN and allow 5 seconds to claim no one is playing
      setTimeout(() => this.doThisAfter(), this.timeOutTime);
    }
  }

  // Receive deck and pile from other users
  doPrepare(msg: any): void {
    if (this.myName.toLowerCase() == msg.username.toLowerCase()) {
      this.initializeCards();

      const receivedDeck = JSON.parse(msg.usersAndCards);
      console.log(receivedDeck);

      const getPiled = this.cards.filter(c => receivedDeck[2] == c.index );
      this.piled = getPiled[0];

      console.log('Other player who gave deck: ' + msg.from);
      const otherUser = {isMe: false, cards: 7, name: msg.from.toUpperCase(), index: this.users.length};
      this.messageAppear(msg.from.toUpperCase() + ' is in the Game! Getting the Deck...');
      this.users.push(otherUser);

      receivedDeck.shift();
      receivedDeck.shift();
      receivedDeck.shift();

      this.cards = receivedDeck.map( c => {
        const getThisCard = this.cards.filter(d => c == d.index );
        console.log(getThisCard);
        return getThisCard[0];
      });
      // this.cards = this.cards.filter(c => receivedDeck.indexOf(c.index) >= 1);
      console.log('Cards upon Receive of existing Deck');
      console.log(this.cards);
      console.log('LISTENING! KNOCK KNOCK in 5 seconds.' + this.whichComesFirst++);

    }
  }

  doThisAfter() { // 5 seconds
    this.connectionSuccess=true;
    console.log('is it after five seconds? ' + this.whichComesFirst++);
    let isFirstUser: boolean;


    if (this.cards.length <= 0) {
      isFirstUser = true;
      this.initializeCards();

      //console.log(this.cards.filter(c=>c.index==58));

      this.myCards = this.getShuffledDeck(7, true);
    } else {
      this.myCards = this.getShuffledDeck(7, false);
    } // supposed to be false

    /*this.myCards[6].face=FACES.PLUS4;
    this.myCards[6].color=null;*/

    //this.checkIfNoMoves(this.myCards);

    // add user to user queue
    this.iam.isMe = true;
    this.iam.cards = this.myCards.length;
    this.iam.name = this.myName.toUpperCase();
    this.iam.index = this.users.length;
    this.users.push(this.iam);

    if (isFirstUser) {
      this.drawAndPile();
    } else {
      // sending back the deck
      const toSend = this.cards.map(x => x.index + '');
      toSend.push(this.myName);
      this.sendMessage('updateDeck', JSON.stringify(toSend.reverse()));
    }
  }

  doUpdateDeck(msg: any): void {
    if (this.myName.toLowerCase() != msg.username.toLowerCase()) {
      const receivedDeck = JSON.parse(msg.usersAndCards);
      console.log(receivedDeck);

      /*console.log(this.cards);
      console.log(this.cards.filter(c => receivedDeck.indexOf(c.index + '') >= 1));*/
      this.cards = this.cards.filter(c => receivedDeck.indexOf(c.index + '') >= 1);
      console.log('Deck updated');
    }
  }

  initializeWebSocketConnection(): void {

    try {
      const ws = new SockJS(this.serverUrl);
      console.log(ws);
      this.stompClient = Stomp.over(ws);
      console.log(this.stompClient)

      this.stompClient.connect({}, frame => {
        this.stompClient.subscribe('/sprite', (message) => {

          console.log('hey  ' + message.body);

          if (message.body) {
            let msg = message.body;

            if ((typeof msg) == 'string') {
              msg = JSON.parse(msg);
            }

            if (msg.action == 'login') {
              this.doLogin(msg);
            } else if (msg.action == 'prepare') {
              this.doPrepare(msg);
            } else if (msg.action == 'updateDeck') {
              this.doUpdateDeck(msg);
            }
          }

        });
      });
    } catch (e) {
      console.log(e);
    }

  }

  sendMessage(act: string, message: any) {
    try {
      switch (act) {
      case 'login':
        this.stompClient.send('/app/login/username', {}, message);
          break;
        case 'prepare':
          this.stompClient.send('/app/prepare/usersAndCards' , {}, message);
          break;
        case 'updateDeck':
          this.stompClient.send('/app/update/deck' , {}, message);
          break;
        case 'place':
          this.stompClient.send('/app/place/card' , {}, message);
          break;
      }
    } catch (e) {
      console.log(e);
    }

  }

  ngOnInit() {
    //this.messageAppear('Multi-player still in progress...');
    this.initAnimate(1);

    this.route.params.subscribe(
      params => {
        this.myName = params.name;
        setTimeout(() => {
          try {
            this.sendMessage('login', this.myName.toUpperCase());
            setTimeout(()=>{
              console.log(this.connectionSuccess)
              if(!this.connectionSuccess)
                this.doLogin({username:this.myName});
            },10000);
          } catch (e) {
            console.log(e);
          }
        }, this.timeOutTime);
      });


    /// this.initializeCards();
    // this.myCards = this.getShuffledDeck(7);
  }

  sendMyCardsForPrepareRequest(): void {
    console.log('SEND REQUEST FOR PREPARE FOR NEW LOGGED IN USER');
  }

  /*isTherAnyone():void{

  }*/

  getValidExceptPlusFour(): boolean{
    const notPlus4:Card[] = this.myCards.filter(c=>c.face!=FACES.PLUS4);
    console.log(notPlus4);
    for(const i in notPlus4){
      console.log(notPlus4[i]);
      console.log(this.checkIfAllowed(notPlus4[i]));

      if(this.checkIfAllowed(notPlus4[i])==ISALLOWED.NORMAL){
        console.log('not Allowed')
        return false;
      }
    }
    console.log('allowed');
    return true;
  }

  checkIfAllowed(card: Card) :string{
    //console.log(card);
    //console.log(this.piled);
    let p = this.piled;

    /*if(card.face==FACES.ANY || card.face==FACES.WILDCARD
      || (typeof +card.face=='number' && +card.face == +p.face)
      || (typeof card.face=='string' && card.face == p.face)
      || card.color == p.color
      || card.face==FACES.PLUS4 && this.getValidExceptPlusFour()
      )
      return ISALLOWED.NORMAL;
    else if(card.face==FACES.PLUS4 && !this.getValidExceptPlusFour()){
      return 'normalHelp';
    }*/

    if(card.face!=FACES.ANY && card.face!=FACES.WILDCARD && card.face!=FACES.PLUS4
      && card.face != p.face
      && card.color != p.color){
      return ISALLOWED.NOTALLOWED;
    }
    else if(card.face==FACES.PLUS4 && !this.getValidExceptPlusFour()){
      return ISALLOWED.NORMALHELP;
    }

    this.noMoves=false;
    return ISALLOWED.NORMAL;
  }

  checkZero(face: string):boolean {
    if(face == FACES.ZERO)
      return true;
    return false;
  }

  getFaceIfPlusFour(card: Card) {
    return card.face==FACES.PLUS4;
  }
}
