package com.example.backend2.controllers;

import com.example.backend2.models.UsersAndDek;
import jdk.nashorn.internal.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

@Controller
public class SocketController {
	/*@MessageMapping("/act")
    @SendTo("/app/sprite")
    public Map progress(Map act){
        //Thread.sleep(1000); // simulated delay
        return act;
    }*/

	private final SimpMessagingTemplate template;


	@Autowired
	SocketController(SimpMessagingTemplate template){
		this.template = template;
	}

	@MessageMapping("/login/username")
	public void onReceivedMessage(String username){
		Map response = new HashMap();
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		response.put("action", "login");
		response.put("username", username);

		System.out.println(response.toString());

		this.template.convertAndSend("/sprite", response);
	}


	@MessageMapping("/update/deck")
	public void updateDeck(String deck){
		Map response = new HashMap();
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		response.put("action", "updateDeck");
		response.put("username", deck.substring(2, deck.indexOf(',')-1));
		response.put("usersAndCards", deck);

		System.out.println(response.toString());

		this.template.convertAndSend("/sprite", response);
	}

	@MessageMapping("/prepare/usersAndCards")
	public void usersAndCards(String usersAndDek){
		//System.out.println(usersAndDek.toString());
		String cloneParam = new String(usersAndDek);


		Map response = new HashMap();
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		response.put("action", "prepare");
		response.put("username", cloneParam.substring(2, usersAndDek.indexOf(',')-1));
		cloneParam = cloneParam.substring(cloneParam.indexOf(',')+2, cloneParam.length());
		response.put("from", cloneParam.substring(0, cloneParam.indexOf(',')-1));
		response.put("usersAndCards", usersAndDek);

		System.out.println(response.toString());

		this.template.convertAndSend("/sprite", response);
	}


	@MessageMapping("/place/card")
	public void placeCards(Map card){
		/*Map result =  new HashMap();
		result.put("userId", userId);
		result.put("rotation", rotation);
		result.put("card", card);*/

		this.template.convertAndSend("/sprite", card);
	}
}
