package com.example.backend2.models;

import lombok.Data;

@Data
public class Card {
    private String color;
    private String face;
}
