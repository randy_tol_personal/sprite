package com.example.backend2.models;

import lombok.Data;

import java.util.List;

@Data
public class UsersAndDek {
    private List<Card> deck;
    private List<Users> users;
}
