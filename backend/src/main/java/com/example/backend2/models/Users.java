package com.example.backend2.models;

import lombok.Data;

@Data
public class Users {
    private int cards;
    private String name;
}
